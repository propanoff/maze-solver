package org.assessment.maze.solver;

import org.assessment.maze.model.Coordinate;
import org.assessment.maze.model.Maze;

import java.util.*;

public class BfsMazeSolver implements MazeSolver {

    @Override
    public List<Coordinate> solve(Maze maze) {
        Queue<Coordinate> nextOnTheRoute = new ArrayDeque<>();
        Coordinate start = maze.getStartLocation();
        nextOnTheRoute.add(start);

        while (!nextOnTheRoute.isEmpty()) {
            Coordinate current = nextOnTheRoute.remove();

            if (!maze.isCoordinateValid(current) || maze.isCoordinateVisited(current)) {
                continue;
            }

            maze.markCoordinateVisited(current);

            if (maze.isCoordinateWall(current)) {
                continue;
            }

            if (maze.isCoordinateExit(current)) {
                return buildPath(current);
            }

            for (int[] direction : Maze.DIRECTIONS) {
                nextOnTheRoute.add(Coordinate.builder()
                        .x(current.getX() + direction[0])
                        .y(current.getY() + direction[1])
                        .parent(current)
                        .build());
            }
        }

        return Collections.emptyList();
    }

    private List<Coordinate> buildPath(Coordinate current) {
        List<Coordinate> path = new ArrayList<>();
        Coordinate copy = current;

        while (copy != null) {
            path.add(copy);
            copy = copy.getParent();
        }

        return path;
    }
}
