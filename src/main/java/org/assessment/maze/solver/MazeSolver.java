package org.assessment.maze.solver;

import org.assessment.maze.model.Coordinate;
import org.assessment.maze.model.Maze;

import java.util.List;

public interface MazeSolver {
    List<Coordinate> solve(Maze maze);
}
