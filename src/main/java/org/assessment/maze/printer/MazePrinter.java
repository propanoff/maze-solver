package org.assessment.maze.printer;

import org.assessment.maze.model.Coordinate;

import java.util.List;

public interface MazePrinter {
    char[][]  getVisualizedSolution(char[][] maze, List<Coordinate> path);
    void print(char[][] maze);
}
