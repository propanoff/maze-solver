package org.assessment.maze.printer;

import org.assessment.maze.model.Coordinate;
import org.assessment.maze.model.Maze;

import java.util.Arrays;
import java.util.List;

public class DefaultConsoleMazePrinter implements MazePrinter {

    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    public char[][] getVisualizedSolution(char[][] maze, List<Coordinate> path) {
        char[][] clonedMaze = maze.clone();
        path.forEach(coordinate -> {
            if (clonedMaze[coordinate.getY()][coordinate.getX()] == Maze.ROAD) {
                clonedMaze[coordinate.getY()][coordinate.getX()] = Maze.ROUTE;
            }
        });

        return clonedMaze;
    }

    public void print(char[][] maze) {
        Arrays.stream(maze)
                .forEach(
                        (row) -> {
                            System.out.print("[");
                            for (char el : row) {
                                System.out.print(" "
                                        + (el == Maze.ROUTE ? ANSI_GREEN + el + ANSI_RESET : el)
                                        + " ");
                            }
                            System.out.println("]");
                        }
                );
    }
}
