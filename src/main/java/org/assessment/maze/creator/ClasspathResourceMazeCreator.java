package org.assessment.maze.creator;

import org.assessment.maze.model.Coordinate;
import org.assessment.maze.model.Maze;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClasspathResourceMazeCreator implements MazeCreator {
    private static final String MAZE_LOCATION = "maze.txt";

    public Maze getMazeStructure() {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(MAZE_LOCATION);
             Scanner sc = new Scanner(is)) {
            List<char[]> rows = new ArrayList<>();

            Maze.MazeBuilder mazeBuilder = Maze.builder();

            while (sc.hasNext()) {
                String mazeRowLine = sc.nextLine();
                if (!mazeRowLine.matches("^[ SFX]*$")) {
                    throw new RuntimeException("Unknown character");
                }
                if (mazeRowLine.indexOf(Maze.START) != -1) {
                    mazeBuilder.startLocation(Coordinate
                            .builder()
                            .x(mazeRowLine.indexOf(Maze.START))
                            .y(rows.size())
                            .build()
                    );
                }
                if (mazeRowLine.indexOf(Maze.EXIT) != -1) {
                    mazeBuilder.finishCoordinate(Coordinate
                            .builder()
                            .x(mazeRowLine.indexOf(Maze.EXIT))
                            .y(rows.size())
                            .build()
                    );
                }
                rows.add(mazeRowLine.toCharArray());
            }

            mazeBuilder.mazeStructure(rows.toArray(new char[0][0]));
            mazeBuilder.visitedCoordinates(new boolean[rows.size()][rows.iterator().next().length]);

            return mazeBuilder.build();
        } catch (IOException e) {
            System.err.println("Unable to parse file with Maza structure: " + MAZE_LOCATION);
            throw new RuntimeException(e);
        }
    }
}
