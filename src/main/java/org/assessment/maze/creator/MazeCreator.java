package org.assessment.maze.creator;

import org.assessment.maze.model.Maze;

public interface MazeCreator {
    Maze getMazeStructure();
}
