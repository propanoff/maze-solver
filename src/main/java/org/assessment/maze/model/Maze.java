package org.assessment.maze.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Maze {
    public static final char START = 'S';
    public static final char EXIT = 'F';
    public static final char WALL = 'X';
    public static final char ROAD = ' ';

    public static final char ROUTE = '•';

    private static final int[] DIRECTION_NORTH = {0, -1};
    private static final int[] DIRECTION_SOUTH = {0, 1};
    private static final int[] DIRECTION_WEST = {1, 0};
    private static final int[] DIRECTION_EAST = {-1, 0};

    public static final int[][] DIRECTIONS = {DIRECTION_NORTH, DIRECTION_SOUTH,
            DIRECTION_WEST, DIRECTION_EAST};

    private char[][] mazeStructure;
    private boolean[][] visitedCoordinates;
    private Coordinate startLocation;
    private Coordinate finishCoordinate;

    public boolean isCoordinateValid(Coordinate coordinate) {
        return coordinate.getX() >= 0
                && coordinate.getX() <= mazeStructure[0].length
                && coordinate.getY() >= 0
                && coordinate.getY() <= mazeStructure.length;
    }

    public boolean isCoordinateVisited(Coordinate coordinate) {
        return visitedCoordinates[coordinate.getY()][coordinate.getX()];
    }

    public boolean isCoordinateWall(Coordinate coordinate) {
        return mazeStructure[coordinate.getY()][coordinate.getX()] == WALL;
    }

    public boolean isCoordinateExit(Coordinate coordinate) {
        return mazeStructure[coordinate.getY()][coordinate.getX()] == EXIT;
    }

    public void markCoordinateVisited(Coordinate coordinate) {
        visitedCoordinates[coordinate.getY()][coordinate.getX()] = true;
    }

}
