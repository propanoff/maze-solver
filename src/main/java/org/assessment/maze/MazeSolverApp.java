package org.assessment.maze;

import org.assessment.maze.creator.ClasspathResourceMazeCreator;
import org.assessment.maze.creator.MazeCreator;
import org.assessment.maze.model.Coordinate;
import org.assessment.maze.model.Maze;
import org.assessment.maze.printer.DefaultConsoleMazePrinter;
import org.assessment.maze.printer.MazePrinter;
import org.assessment.maze.solver.BfsMazeSolver;
import org.assessment.maze.solver.MazeSolver;

import java.util.List;

public class MazeSolverApp {
    public static void main(String[] args) {
        MazeCreator mazeCreator = new ClasspathResourceMazeCreator();
        Maze maze = mazeCreator.getMazeStructure();

        MazePrinter mazePrinter = new DefaultConsoleMazePrinter();

        System.out.println("Original Maze");
        mazePrinter.print(maze.getMazeStructure());

        MazeSolver mazeSolver = new BfsMazeSolver();
        List<Coordinate> solvedPath = mazeSolver.solve(maze);

        char[][] visualizedSolution = mazePrinter.getVisualizedSolution(maze.getMazeStructure(), solvedPath);

        System.out.println("Found solution");
        mazePrinter.print(visualizedSolution);
    }

}
