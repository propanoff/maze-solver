# Maze solver demo project

## About the application

* It is a console Java application
* It solves the maze which is described in application's resources:
> src/main/resources/maze.txt
* It uses BFS algorithm
* It prints the result to the application console
* Found route is marked with green dots •

## Build

> mvn clean package

## Run

> java -jar target/maze-solver-1.0-SNAPSHOT-jar-with-dependencies.jar 

##Example of the output

```
Original Maze
[ X  X  X  S  X ]
[ X  X  X     X ]
[ X  X        X ]
[ X        X  X ]
[ X  X  F  X  X ]
Found solution
[ X  X  X  S  X ]
[ X  X  X  •  X ]
[ X  X  •  •  X ]
[ X     •  X  X ]
[ X  X  F  X  X ]

```
